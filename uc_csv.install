<?php

/**
 * @file
 * This defines our schema for the module
 */

/**
 * Implementation of hook_schema()
 */
function uc_csv_schema() {
  $schema['uc_csv_reports'] = array(
    'description' => 'A table of configured reports',
    'fields' => array(
      'rid' => array(
        'description' => 'The export report key.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE),
      'report_name' => array(
        'description' => 'The name of the report',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE),
      'last_exported' => array(
        'description' => 'The date of the last export',
        'type' => 'datetime',
        'size' => 'normal',
        'not null' => TRUE),
      'last_order_id' => array(
        'description' => 'The last order id exported',
        'type' => 'int',
        'length' => 11,
        'not null' => TRUE),
      'shipping_address' => array(
        'description' => 'Include shipping report in export.',
        'type' => 'int',
        'length' => 1,
        'not null' => TRUE),
      'billing_address' => array(
        'description' => 'Include billing address in export',
        'type' => 'int',
        'length' => 1,
        'not null' => TRUE),
      'products' => array(
        'description' => 'Include products in export',
        'type' => 'int',
        'length' => 1,
        'not null' => TRUE),
      'orderby' => array(
        'description' => 'How the report is to be ordered',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE),
    ),
    'primary key' => array('rid'),
  );
  return $schema;
}

/** 
 * implementation of hook_install()
 */
function uc_csv_install() {
  drupal_install_schema('uc_csv');
}

/** 
 * implementation of hook_uninstall()
 */
function uc_csv_uninstall() {
  drupal_uninstall_schema('uc_csv');
}
